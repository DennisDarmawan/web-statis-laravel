<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration</title>
</head>
    <style>
        body{
            font-family: sans-serif;
        }
    </style>
<body>
    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>
    
    <form action="/Welcome">
        @csrf
        <p>First Name:</p>
        <input type="text" name="fistname">

        <p>Last Name:</p>
        <input type="text" name="lastname">

        <p>Gender:</p>
        <input type="radio" name="gender" value="0"> Male <br>
        <input type="radio" name="gender" value="0"> Female <br>
        <input type="radio" name="gender" value="0"> Other <br>

        <p>Nationality:</p>
        <select name="" id="">
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select>

        <p>Language Spoken:</p>
        <input type="checkbox" name="lang" value="0"> Bahasa Indonesia <br>
        <input type="checkbox" name="lang" value="0"> English <br>
        <input type="checkbox" name="lang" value="0"> Other <br>

        <p>Bio:</p>
        <textarea cols="30" rows="10"></textarea> <br>

        <a href="Welcome"><input type="submit" value="Sign Up"></a>
    </form>
</body>
</html>